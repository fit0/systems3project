<?php

class Features extends CI_Controller
{
	public $CIF = NULL;

	public function __construct()
	{

		parent::__construct();

		$this->load->helper('url_helper');

		// Load form helper library
		$this->load->helper('form');

		$this->load->helper('file');

		// Load form validation library
		$this->load->library('form_validation');

		// Load session library
		$this->load->library('session');

		// Load database
		$this->load->model('login_database');
	}

	public function index()
	{

		$data['logiran'] = isset($this->session->userdata['logged_in']);

		if (!isset($this->session->userdata['logged_in'])) {
			$data['message_display'] = 'Signin to view this page!';
			$this->load->view('templates/header', $data);
			$this->load->view('user_authentication/login_form', $data);
			$this->load->view('templates/footer');
			return;
		}
		$data['logiran'] = isset($this->session->userdata['logged_in']);

		$user = $this->session->userdata['logged_in']['email'];
		$user_id = $this->login_database->getUserIDfromemail($user)[0]->id;

		$posts = $this->login_database->getAllfromone($user_id);
		$this->load->view('templates/header', $data);
		$this->load->view('features/members_area', ['posts' => $posts]);
		$this->load->view('templates/footer');
	}

	public function upload_post()
	{

		$user = $this->session->userdata['logged_in']['email'];
		$user_id = $this->login_database->getUserIDfromemail($user)[0]->id;


		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('sport', 'Sport', 'trim|required');
		$this->form_validation->set_rules('num', 'Number of missing players', 'trim|required');
		$this->form_validation->set_rules('landt', 'Location and time', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'trim|required');

		if ($this->form_validation->run() === FALSE) {

			$this->index();

		} else {

			$data = array(
				'title' => $this->input->post('title'),
				'sport' => $this->input->post('sport'),
				'num' => $this->input->post('num'),
				'landt' => $this->input->post('landt'),
				'description' => $this->input->post('description'),
				'u_id' => $user_id,
			);

			$this->form_validation->set_error_delimiters();

			
			if ($this->login_database->insertpost($data)) {
					redirect('https://www.studenti.famnit.upr.si/~89191046/Systems3-Project/systems3-project/index.php/features/index');
				} else {
					redirect('https://www.studenti.famnit.upr.si/~89191046/Systems3-Project/systems3-project/index.php/features/index');
				}
			
		}

	}

	public function najdi($data)
	{
		return $this->login_database->getUser($data);
	}


	public function delete($postid)
	{

		$u = $this->session->userdata['logged_in']['email'];
		$check = $this->login_database->getUserIDfromemail($u)[0]->id;
		$check2 = $this->login_database->getUserID($postid)[0]->u_id;

		if ($check === $check2) {
			$this->login_database->delete_post($postid);
			$this->index();
		} else {
			echo "You don't have permission to delete this item!";
		}

	}

	public function edit($id)
	{

		$data['logiran'] = isset($this->session->userdata['logged_in']);

		if (!isset($this->session->userdata['logged_in'])) {
			$data['message_display'] = 'Signin to view this page!';
			$this->load->view('templates/header', $data);
			$this->load->view('user_authentication/login_form', $data);
			$this->load->view('templates/footer');
			return;
		}

		$data['logiran'] = isset($this->session->userdata['logged_in']);

		$u = $this->session->userdata['logged_in']['email'];
		$check = $this->login_database->getUserIDfromemail($u)[0]->id;
		$check2 = $this->login_database->getUserID($id)[0]->u_id;

		if ($check === $check2) {

			$data['title'] = 'Update post';

			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('sport', 'Sport', 'required');

			if ($this->form_validation->run() === FALSE) {
				$post = $this->login_database->getPost($id);

				$this->load->view('templates/header', $data);
				$this->load->view('features/edit', ['post' => $post]);
				$this->load->view('templates/footer');

			} else {
				$new["title"] = $this->input->post("title");
				$new["sport"] = $this->input->post("sport");
				$new["num"] = $this->input->post("num");
				$new["landt"] = $this->input->post("landt");
				$new["description"] = $this->input->post("description");
				$this->login_database->update_post($new, $id);
				$this->index();
			}
		} else {

			echo "You dont have permission to edit this video!";
		}
	}


}
