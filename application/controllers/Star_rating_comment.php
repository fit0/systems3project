<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Star_rating_comment extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');

		// Load session
		$this->load->library('session');
		$this->load->library('form_validation');

		// Load model
		$this->load->model('star_rating_comment_model');
		$this->load->model('login_database');

	}

	public function index()
	{

		$data = array();

		$data['logiran'] = isset($this->session->userdata['logged_in']);

		if (!isset($this->session->userdata['logged_in'])) {
			$data['message_display'] = 'Signin to view this page!';
			$this->load->view('templates/header', $data);
			$this->load->view('user_authentication/login_form', $data);
			$this->load->view('templates/footer');
			return;
		}
		$data['logiran'] = isset($this->session->userdata['logged_in']);


		// Userid
		$user = $this->session->userdata['logged_in']['email'];
		$userid = $this->login_database->getUserIDfromemail($user)[0]->id;

		// Fetch all records
		$data['posts'] = $this->star_rating_comment_model->getAllPosts($userid);
		$this->load->view('templates/header', $data);
		$this->load->view('features/display', ['data' => $data]);
		$this->load->view('templates/footer');

	}

	public function display($data)
	{
		$data['logiran'] = isset($this->session->userdata['logged_in']);

		$this->load->view('templates/header', $data);
		$this->load->view('features/display', ['data' => $data]);
		$this->load->view('templates/footer');
	}

	// Update rating
	public function updateRating()
	{

		// userid
		$user = $this->session->userdata['logged_in']['email'];
		$userid = $this->login_database->getUserIDfromemail($user)[0]->id;

		// POST values
		$post_id = $this->input->post('post_id');
		$rating = $this->input->post('rating');

		// Update user rating and get Average rating of a post
		$averageRating = $this->star_rating_comment_model->userRating($userid, $post_id, $rating);

		echo $averageRating;
		exit;
	}

	public function leave_comment()
	{

		$user = $this->session->userdata['logged_in']['email'];
		$user_id = $this->login_database->getUserIDfromemail($user)[0]->id;

		$post_id = $this->input->post('post_id');
		$text = $this->input->post('text');

		$comment = $this->star_rating_comment_model->publish_comment($post_id, $user_id, $text);

	}

	public function searchfor()
	{

		$user = $this->session->userdata['logged_in']['email'];
		$userid = $this->login_database->getUserIDfromemail($user)[0]->id;

		$keyword = $this->input->post('text');

		$data['posts'] = $this->star_rating_comment_model->searchvideos($userid, $keyword);

		$response = $this->display($data);

		echo $response;
	}

	public function check()
	{

		$u = $this->session->userdata['logged_in']['email'];
		$check = $this->login_database->getUserIDfromemail($u)[0]->id;

		$commentid = $this->input->post('comment_id');

		$check2 = $this->star_rating_comment_model->getUserIDfromComment($commentid)[0]->user_id;

		if ($check === $check2) {
			echo "true";
		} else {
			echo "false";
		}

	}

	public function delete_comment() {
		$commentid = $this->input->post('comment_id');
		$this->star_rating_comment_model->delete_comment($commentid);
	}

}
