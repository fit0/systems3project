<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Star_rating_comment_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	// Fetch records
	public function getAllPosts($userid)
	{

		// Posts
		$selection = 'a.id, a.title, a.sport, a.description, a.landt, a.date, a.num, a.u_id, u.name, u.surname';
		$fromtables = 'adss a, user u';
		$condition = 'a.u_id = u.id';
		$this->db->select($selection);
		$this->db->from($fromtables);
		$this->db->where($condition);
		$this->db->order_by("a.id", "desc");
		$postsquery = $this->db->get();

		$postResult = $postsquery->result_array();

		$posts_arr = array();
		foreach ($postResult as $post) {
			$id = $post['id'];
			$title = $post['title'];
			$sport = $post['sport'];
			$description = $post['description'];
			$landt = $post['landt'];
			$date = $post['date'];
			$num = $post['num'];
			$user_id = $post['u_id'];
			$name = $post['name'];
			$surname = $post['surname'];

			// User rating
			$this->db->select('stars');
			$this->db->from('rating');
			$this->db->where("u_id", $userid);
			$this->db->where("post_id", $id);
			$userRatingquery = $this->db->get();

			$userpostResult = $userRatingquery->result_array();

			$userRating = 0;
			if (count($userpostResult) > 0) {
				$userRating = $userpostResult[0]['stars'];
			}

			// Average rating
			$this->db->select('ROUND(AVG(stars),1) as averageRating');
			$this->db->from('rating');
			$this->db->where("post_id", $id);
			$ratingquery = $this->db->get();

			$postResult = $ratingquery->result_array();

			$rating = $postResult[0]['averageRating'];

			if ($rating == '') {
				$rating = 0;
			}

			// Comments
			$condition = 'u.id = c.u_id AND c.post_id =' . $id;
			$this->db->select('name, surname, text, date, c.id');
			$this->db->from('user u, comment c');
			$this->db->where($condition);
			$commentsquery = $this->db->get();

			$commentsResult = $commentsquery->result_array();

			$posts_arr[] = array("id" => $id, "title" => $title, "sport" => $sport, "num" => $num, "description" => $description, "landt" => $landt, "date" => $date, "u_id" => $user_id, "name" => $name, "surname" => $surname, "rating" => $userRating, "averagerating" => $rating, "comments" => $commentsResult);
		}

		return $posts_arr;
	}

	// Save user rating
	public function userRating($userid, $post_id, $rating)
	{
		$this->db->select('*');
		$this->db->from('rating');
		$this->db->where("post_id", $post_id);
		$this->db->where("u_id", $userid);
		$userRatingquery = $this->db->get();

		$userRatingResult = $userRatingquery->result_array();
		if (count($userRatingResult) > 0) {

			$postRating_id = $userRatingResult[0]['id'];
			// Update
			$value = array('stars' => $rating);
			$this->db->where('id', $postRating_id);
			$this->db->update('rating', $value);
		} else {
			$userRating = array(
				"post_id" => $post_id,
				"u_id" => $userid,
				"stars" => $rating
			);

			$this->db->insert('rating', $userRating);
		}

		// Average rating
		$this->db->select('ROUND(AVG(stars),1) as averageRating');
		$this->db->from('rating');
		$this->db->where("post_id", $post_id);
		$ratingquery = $this->db->get();

		$postResult = $ratingquery->result_array();

		$rating = $postResult[0]['averageRating'];

		if ($rating == '') {
			$rating = 0;
		}
		return $rating;
	}

	public function publish_comment($post_id, $user_id, $text)
	{

		$post_data = array(
			'post_id' => $post_id,
			'u_id' => $user_id,
			'text' => $text,
		);

		$this->db->insert('comment', $post_data);
	}

	public function searchvideos($userid, $keyword)
	{
		$selection = 'a.id, a.title, a.sport, a.date, a.description, a.landt, a.num, a.u_id, u.name, u.surname';
		$fromtables = 'adss a, user u';
		$condition = "a.u_id = u.id and (a.title like '%" . $keyword . "%' or a.sport like '%" . $keyword . "%' or a.num like '%" . $keyword . "%' or a.landt like '%" . $keyword . "%')";
		$this->db->select($selection);
		$this->db->from($fromtables);
		$this->db->where($condition);
		$postsquery = $this->db->get();

		$postResult = $postsquery->result_array();

		$posts_arr = array();
		foreach ($postResult as $post) {
			$id = $post['id'];
			$title = $post['title'];
			$sport = $post['sport'];
			$date = $post['date'];
			$description = $post['description'];
			$landt = $post['landt'];
			$num = $post['num'];
			$user_id = $post['u_id'];
			$name = $post['name'];
			$surname = $post['surname'];

			// User rating
			$this->db->select('stars');
			$this->db->from('rating');
			$this->db->where("u_id", $userid);
			$this->db->where("post_id", $id);
			$userRatingquery = $this->db->get();

			$userpostResult = $userRatingquery->result_array();

			$userRating = 0;
			if (count($userpostResult) > 0) {
				$userRating = $userpostResult[0]['stars'];
			}

			// Average rating
			$this->db->select('ROUND(AVG(stars),1) as averageRating');
			$this->db->from('rating');
			$this->db->where("post_id", $id);
			$ratingquery = $this->db->get();

			$postResult = $ratingquery->result_array();

			$rating = $postResult[0]['averageRating'];

			if ($rating == '') {
				$rating = 0;
			}

			// Comments
			$condition = 'u.id = c.u_id AND c.post_id =' . $id;
			$this->db->select('name, surname, text, date, c.id');
			$this->db->from('user u, comment c');
			$this->db->where($condition);
			$commentsquery = $this->db->get();

			$commentsResult = $commentsquery->result_array();

			$posts_arr[] = array("id" => $id, "title" => $title, "sport" => $sport, "description" => $description, "landt" => $landt, "num" => $num, "date" => $date, "u_id" => $user_id, "name" => $name, "surname" => $surname, "rating" => $userRating, "averagerating" => $rating, "comments" => $commentsResult);
		}

		return $posts_arr;
	}

	public function delete_comment($commentid)
	{

		$this->db->where("id", $commentid);
		$this->db->delete("comment");
	}

	public function getUserIDfromComment($data)
	{
		$condition = "id =" . "'" . $data . "'";
		$this->db->select('u_id');
		$this->db->from('comment');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		return $query->result();
	}
}
