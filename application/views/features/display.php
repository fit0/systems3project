<div class="row justify-content-center">
	<div class="col-12 col-md-10 col-lg-8">
		<div class="card card-sm">
			<div class="card-body row no-gutters align-items-center">
				<div class="col-auto">
					<i class="fa fa-search h4 text-body"></i>
				</div>
				<!--end of col-->
				<div class="col">
					<input class="form-control form-control-lg form-control-borderless" id="textarea" type="search"
						   placeholder="Search sports or keywords">
				</div>
				<!--end of col-->
				<div class="col-auto">
					<button class="btn btn-lg btn-success" id="searchbutton" type="submit">Search</button>
				</div>
				<!--end of col-->
			</div>
		</div>
	</div>
	<!--end of col-->
</div>
<div class="row">
	<div class="col">
		<?php if (count($data['posts'])) : ?>
			<?php foreach ($data['posts'] as $post) : ?>
				<div>
					<!-- Post Content Column -->
					<div>
						<!-- Title -->
						<h1 class="mt-4"><?php echo $post['title']; ?></h1>
						<p class="lead">
							Sport: <?php echo $post['sport']; ?>
						</p>
						<p class="lead">
							Number of missing players: <?php echo $post['num']; ?>
						</p>
						<p class="lead">
							Location and time: <?php echo $post['landt']; ?>
						</p>
						<p class="lead">
							Description: <?php echo $post['description']; ?>
						</p>
						<!-- Author -->
						<p class="lead">
							by <?php echo $post['name'] . " " . $post['surname']; ?>
						</p>
					</div>
				</div>
			</div>
		</div>

						<!-- Date/Time -->
						<div class="row">
							<div class="col">
								<p><?php echo $post['date']; ?></p>
							</div>
						</div>
						

						<div class="">
							<div class="row">
								<div class="col">
									<div class="post" style="padding: 200px;">
										<div class="post-action" style="margin-top: -290px;">
											<!-- Rating Bar -->
											<input id="post_<?= $post['id'] ?>" value='<?= $post['rating'] ?>'
												   class="rating-loading ratingbar" data-min="0" data-max="5"
												   data-step="1">
											<!-- Average Rating -->
											<div>Average User Rating: <span
														id='averagerating_<?= $post['id'] ?>'><?= $post['averagerating'] ?></span>
											</div>
										</div>
									</div>
								</div>

                                
                                
								<div class="col">
									<div class="container d-flex justify-content-center mt-100 mb-100">
										<div class="row">
											<div class="col-md-12">
												<div class="card" style="margin-top: -350px;">
													<div class="card-body">
														<h4 class="card-title">Recent Mesages</h4>
														<h6 class="card-subtitle">Latest Messages section by users</h6>
													</div>
													<div class="comment-widgets m-b-20"
														 style="overflow-y: scroll; height:400px; width: 400px; ">
														<?php foreach ($post['comments'] as $comment) : ?>
															<div class="d-flex flex-row comment-row">
																<div class="comment-text w-100">
																	<h5><?php echo $comment['name'] . " " . $comment['surname']; ?></h5>
																	<div class="comment-footer">
																		<span class="date">Posted on <?php echo $comment['date'] ?></span>
																		<span class="action-icons">
                                                                            <a class="commentedit"
																			   id="edit_<?= $comment['id'] ?>"
																			   data-abc="true"><i
																						class="fa fa-trash"></i></a>
                                                                        </span>
																	</div>
																	<p class="m-b-5 m-t-10"><?php echo $comment['text']; ?></p>
																</div>
															</div>
														<?php endforeach; ?>
														<hr>
													</div>
													<div>Leave a comment</div>
													<textarea class="textarea" id="text_<?= $post['id'] ?>" name="text"
															  required></textarea>
														
													<input class="komentar" id="postcomment_<?= $post['id'] ?>"
														   type="submit">
														
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>
				</div>
			<?php endforeach; ?>
		<?php else : ?>
			<div class="alert alert-primary" role="alert">
				No such ads in the database!
			</div>
		<?php endif; ?>
	</div>
</div>
<!-- Script -->
<script type='text/javascript'>

	$(document).ready(function () {
		// Initialize
		$('.ratingbar').rating({
			showCaption: false,
			showClear: false,
			size: 'sm'
		});

		// Rating change
		$('.ratingbar').on('rating:change', function (event, value, caption) {
			var id = this.id;
			var splitid = id.split('_');
			var post_id = splitid[1];

			$.ajax({
				url: '<?= base_url() ?>index.php/star_rating_comment/updateRating',
				type: 'post',
				data: {
					post_id: post_id,
					rating: value,
				},
				success: function (response) {
					$('#averagerating_' + post_id).text(response);
				}
			});
		});
	});
	$(document).ready(function () {

		$('.komentar').click(function (event, value, caption) {

			var id = this.id;
			var splitid = id.split('_');
			var post_id = splitid[1];
			var text = $("#text_" + post_id).val();
			if (text == '') {
				alert("Please enter your comment");
			} else {
				$.ajax({
					url: '<?= base_url() ?>index.php/star_rating_comment/leave_comment',
					type: 'post',
					data: {
						post_id: post_id,
						text: text,
					},
					success: function (response) {
						setTimeout(function () {
							location.reload();
						}, 500);
					}
				});
			}
		})
	});

	$(document).ready(function () {

		$('#searchbutton').click(function (event, value, caption) {
			var text = $("#textarea").val();
			if (text == '') {
				alert("Please review your search parameters");
			} else {
				$.ajax({
					url: '<?= base_url() ?>index.php/star_rating_comment/searchfor',
					type: 'post',
					dataType: "html",
					data: {
						text: text,
					},
					success: function (response) {
						$('body').html(response);
					},
					error: function (result) {
						$('body').html("err");
					},
					beforeSend: function (d) {
						$('body').html("Searching...");
					}
				});
			}
		})
	});

	$(document).on("click", "a.commentedit", function () {

		var id = this.id;
		var splitid = id.split('_');
		var comment_id = splitid[1];
		$.ajax({
			url: '<?= base_url() ?>index.php/star_rating_comment/check',
			type: 'post',
			data: {
				comment_id: comment_id,
			},
			success: function (response) {
				if (response === 'true') {
					if (confirm('Are you sure ?')) {
						$.ajax({
							url: '<?= base_url() ?>index.php/star_rating_comment/delete_comment',
							type: 'post',
							data: {
								comment_id: comment_id,
							},
							success: function (response) {
								setTimeout(function () {
									location.reload();
								}, 500);
							}
						});
					}
				} else {
					alert("You dont have permission!");
				}
			}
		});

	});

</script>
