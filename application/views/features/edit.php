<div>
	<!-- Post Content Column -->
	<div>
		<div class="container">
			<div class="row">
				<div class="col">
					<h1 class="mt-4"><?php echo $post['title']; ?></h1>
					<hr>
					<p class="lead">
						Sport: <?php echo $post['sport']; ?>
					</p>
					<p class="lead">
						Number of missing players: <?php echo $post['num']; ?>
					</p>
					<p class="lead">
						Location and time: <?php echo $post['landt']; ?>
					</p>
					<p class="lead">
						Description: <?php echo $post['description']; ?>
					</p>
					<hr>
					<!-- Date/Time -->
					<div class="row">
						<div class="col">
							<p><?php echo $post['date']; ?></p>
						</div>
					</div>
					<hr>
				</div>
				<div class="col">
					<?php echo form_open('features/edit/'.$post['id']) ?>
					<div class="form-group"><label for="title">Title</label>
						<br/>
						<?php
						$data1 = array(
								'type' => 'text',
								'name' => 'title',
								'class' => 'form-control item',
						);
						echo form_input($data1); ?>
						<br/>
					</div>
					<div class="form-group"><label for="sport">Sport</label>
						<br/>
						<?php
						$data2 = array(
								'type' => 'text',
								'name' => 'sport',
								'class' => 'form-control item',
						);
						echo form_input($data2); ?>
						<br/>
					</div>
					<div class="form-group"><label for="num">Number of missing players</label>
						<br/>
						<?php
						$data4 = array(
								'type' => 'text',
								'name' => 'num',
								'class' => 'form-control item'
						);
						echo form_input($data4); ?>
						<br/>
					</div>
					<div class="form-group"><label for="num">Location and time</label>
						<br/>
						<?php
						$data4 = array(
								'type' => 'text',
								'name' => 'landt',
								'class' => 'form-control item'
						);
						echo form_input($data4); ?>
						<br/>
					</div>
					<div class="form-group"><label for="num">Description</label>
						<br/>
						<?php
						$data4 = array(
								'type' => 'text',
								'name' => 'description',
								'class' => 'form-control item'
						);
						echo form_input($data4); ?>
						<br/>
					</div>
					<?php
					$data5 = array(
							'type' => 'submit',
							'name' => 'submit',
							'class' => 'btn btn-primary btn-block',
							'value' => 'Edit',
					);
					echo form_submit($data5);
					echo form_close();
					?>
				</div>
			</div>
		</div>
	</div>
</div>
