<?php

if (!isset($this->session->userdata['logged_in'])) {
	$data['message_display'] = 'Signin to view this page!';
	$this->load->view('user_authentication/login_form', $data);
	return;
} ?>

<div class="row">
	<div class="col">
		<?php if (count($posts)): ?>
			<?php foreach ($posts as $post): ?>
				<div>
					<div>
						<!-- Post Content Column -->
						<div>
							<!-- Title -->
							<h1 class="mt-4"><?php echo $post->title; ?></h1>
							<p class="lead">
								Sport: <?php echo $post->sport; ?>
							</p>
							<p class="lead">
								Number of missing players: <?php echo $post->num; ?>
							</p>
							<p class="lead">
								Location and time: <?php echo $post->landt; ?>
							</p>
							<p class="lead">
								Description: <?php echo $post->description; ?>
							</p>
							<hr>
							<!-- Date/Time -->
							<div class="row">
								<div class="col">
									<p><?php echo $post->date; ?></p>
								</div>
								<div class="col">
									<div class="dropdown" style="float: right">
										<button class="btn btn-secondary dropdown-toggle" type="button"
												id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
												aria-expanded="false">
											Options
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<a class="dropdown-item"
											   href="<?php echo site_url('features/edit/' . $post->id) ?>">Edit</a>
											<a class="dropdown-item"
											   onclick='return confirm("Are you sure you want to delete?");'
											   href="<?php echo site_url('features/delete/' . $post->id); ?>">Delete</a>
										</div>
									</div>
								</div>
							</div>
							<hr>
						</div>
					</div>
					<hr>
				</div>
			<?php endforeach; ?>
		<?php else: ?>
			<div class="alert alert-primary" role="alert">
				You have no ads currently in the database!
			</div>
		<?php endif; ?>
	</div>
	<div class="col">
		<div class="col">
			<div class="block-heading" align="center">
				<h2 class="text-info">Post</h2>
				<p><?php echo "<div class='error_msg'>";
					echo validation_errors();
					echo "</div>";
					if (isset($error_message)) {
						echo $error_message;
					}; ?></p>
			</div>
		</div>

		<?php echo form_open_multipart('features/upload_post') ?>
		
		<div class="form-group"><label for="title">Title</label>
			<br/>
			<?php
			$data2 = array(
					'type' => 'text',
					'name' => 'title',
					'class' => 'form-control item'
			);
			echo form_input($data2); ?>
			<br/>
		</div>
		<div class="form-group"><label for="sport">Sport</label>
			<br/>
			<?php
			$data3 = array(
					'type' => 'text',
					'name' => 'sport',
					'class' => 'form-control item'
			);
			echo form_input($data3); ?>
			<br/>
		</div>
		<div class="form-group"><label for="num">Number of missing players</label>
			<br/>
			<?php
			$data4 = array(
					'type' => 'text',
					'name' => 'num',
					'class' => 'form-control item'
			);
			echo form_input($data4); ?>
			<br/>
		</div>
		<div class="form-group"><label for="landt">Location and time</label>
			<br/>
			<?php
			$data4 = array(
					'type' => 'text',
					'name' => 'landt',
					'class' => 'form-control item'
			);
			echo form_input($data4); ?>
			<br/>
		</div>
		<div class="form-group"><label for="description">Description</label>
			<br/>
			<?php
			$data4 = array(
					'type' => 'text',
					'name' => 'description',
					'class' => 'form-control item'
			);
			echo form_input($data4); ?>
			<br/>
		</div>
		<?php
		$data5 = array(
				'type' => 'submit',
				'name' => 'submit',
				'class' => 'btn btn-primary btn-block',
				'value' => 'Post',
		);
		echo form_submit($data5);
		echo form_close();
		?>
	</div>
</div>

</body>
