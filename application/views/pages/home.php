<main class="page landing-page">
	<section class="clean-block clean-hero" style="background-image:url(<?php echo base_url(); ?>assets/img/tech/image4.jpg&quot;);color:rgba(9, 162, 100, 0.85);">
		<div class="text">
			<h2>Sport Finder</h2>
			<p>We help users to find and connect with other players for their sport.</p><button class="btn btn-outline-light btn-lg" type="button">Learn More</button>
		</div>
	</section>
</main>
