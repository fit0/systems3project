<html>

<head>

	<title><?php if (isset($title)) echo $title; ?></title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<link rel="icon" href="<?php echo base_url()?>/favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/smoothproducts.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link href='<?= base_url() ?>assets/bootstrap-star-rating/css/star-rating.min.css' type='text/css' rel='stylesheet'>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/rating_style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/comment_style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/searchbar_style.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/simple-line-icons.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
	<script src='<?= base_url() ?>assets/js/jquery-3.5.1.js' type='text/javascript'></script>
	<script src='<?= base_url() ?>assets/bootstrap-star-rating/js/star-rating.min.js' type='text/javascript'></script>

</head>

<body>

<nav class="navbar navbar-light navbar-expand-lg position-sticky bg-white clean-navbar">
	<div class="container"><a class="navbar-brand logo" href="<?php echo site_url('pages/view/home'); ?>">Sport Finder</a>
		<button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
		<div class="collapse navbar-collapse" id="navcol-1">
			<ul class="nav navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link" href="<?php echo site_url('pages/view/home'); ?>">Home</a></li>
				<li class="nav-item"><a class="nav-link" href="<?php echo site_url('pages/view/aboutus'); ?>">About Us</a></li>
				<li class="nav-item"><a class="nav-link" href="<?php echo site_url('pages/view/contactus'); ?>">Contact Us</a></li>
				<?php if ($logiran == true) {
					echo '<li class="nav-item"><a class="nav-link" href="';
					echo site_url("star_rating_comment/index") . '"' . '>All Posts</a> </li>';
					echo '<li class="nav-item"><a class="nav-link" href="';
					echo site_url("features/index") . '"' . '>Dashboard</a> </li>';
					echo '<li class="nav-item"><a class="nav-link" style="text-align: right" href="';
					echo site_url("user_authentication/logout") . '"' . '>Logout </a> </li>';
				} else {
					echo '<li class="nav-item"><a class="nav-link" href="';
					echo site_url("user_authentication/index") . '"' . '>Login</a></li>';
					echo '<li class="nav-item"><a class="nav-link" href="';
					echo site_url("user_authentication/show") . '"' . '>Register</a></li>';
				} ?>

			</ul>
		</div>
	</div>
</nav>
